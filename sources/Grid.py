"""
    Class Grid():
    
    Autor : David Wrobel
    Date  : 07 Janvier 2020
    
    Implémentation d'une classe modélisant
    une grille de jeu
"""

from Case import *

class Grid():
    
    def __init__(self, dim):
        """
            dim : tuple (width, height)
        """
        self.__width, self.__height = dim       
        # On créé la grille
        self.__grid = self.__create_grid()
        self.list_player = ()
        
    def get_grid_dimension(self):
        """
            Retourne les dimensions de la grille sous forme d'un tuple
        """
        return self.__width, self.__height
    
    def __create_grid(self):
        """
            Initialise une grille vide aux bonnes dimensions
        """
        w, h = self.get_grid_dimension()
        
        # Dico des coins
        WALL = {(0, 0)     : NW_CORNER,
                (w-1, 0)   : NE_CORNER,
                (0, h-1)   : SW_CORNER,
                (w-1, h-1) : SE_CORNER}
        
        WALL.update({(x, 0)   : NORTH_WALL for x in range(1, w-1)}) # BORD NORD
        WALL.update({(w-1, y) : EAST_WALL  for y in range(1, h-1)}) # BORD EST
        WALL.update({(x, h-1) : SOUTH_WALL for x in range(1, w-1)}) # BORD SUD
        WALL.update({(0, y)   : WEST_WALL  for y in range(1, h-1)}) # BORD OUEST
        
        grid = []
        
        for y in range(h):
            line = []
            for x in range(w):
                # Si la case est un coin ou un bord
                # On récupère le coin ou le bord associé avec le dico                    
                try:
                    line.append(Case(WALL[(x,y)]))
                except KeyError:
                    line.append(Case(NONE_WALL))
                    
            grid.append(line)
            
        return grid
    
    def get_cell(self, coord):
        """
            Accesseur d'une cellule
        """
        x, y = coord
        return self.__grid[y][x]
    
    def set_case_wall(self, coord, wall):
        """
            Création d'un mur
            coord : tuple
            wall  : Mot binaire
        """
        x, y = coord
        
        # Si on doit placer un mur EST
        if wall & EAST_WALL:
            self.get_cell((x, y)).set_wall(EAST_WALL)
            if x < self.__width-1: # Il y a aussi un mur à l'OUEST
                self.get_cell((x+1, y)).set_wall(WEST_WALL)
                w, h = self.get_grid_dimension()
        # Si on doit placer un mur SUD
        if wall & SOUTH_WALL:
            self.get_cell((x, y)).set_wall(SOUTH_WALL)
            if y < self.__height-1: # Il y a aussi un mur au NORD
                self.get_cell((x, y+1)).set_wall(NORTH_WALL)
            
        # Un mur au nord = un mur au sud de la case du dessus
        # Un mur à l'ouest = un mur à l'est de la case de gauche
        if 0 < x:
            if wall & WEST_WALL :
                self.set_case_wall((x-1, y), EAST_WALL)
        if 0 < y:
            if wall & NORTH_WALL:
                self.set_case_wall((x, y-1), SOUTH_WALL)
    
    def get_number_of_player(self):
        return len(self.list_player)
            
    def set_config(self, list_player):
        """
            list_player : tuple de coordonnées
        """
        self.list_player = list_player
        
        num = 0
        for p in list_player:
            self.get_cell(p).set_player(num)
            num += 1
            
    def set_player_pos(self, num, coord):
        self.list_player = self.list_player[:num] + (coord, ) + self.list_player[num+1:]
            
    def get_config(self):
        return self.list_player
        
    def set_final_case(self, coord):
        self.get_cell(coord).set_final()
        
    def get_final_case(self):
        w, h = self.get_grid_dimension()
        
        for y in range(h):
            for x in range(w):
                if self.get_cell((x,y)).is_final():
                    return (x,y)

    """
        Gestion des déplacements.
    """
    def move_player_one_time_by_direction(self, num, direction):
        """
            Déplace d'une seule case le joueur dans la bonne direction
        """
        coord = self.list_player[num]
        x, y = coord
        
        dico_wall = {'N' : NORTH_WALL, 'E' : EAST_WALL, 'S' : SOUTH_WALL, 'W' : WEST_WALL}
        dico_new_coord = {'N' : (x,y-1), 'E' : (x+1,y), 'S' : (x,y+1), 'W' : (x-1,y)}
        
        new_coord = dico_new_coord[direction]
        
        if not(self.get_cell(coord).get_wall() & dico_wall[direction]) and self.get_cell(new_coord).is_free():
            if num != 0:
                if not(self.get_cell(new_coord).is_final()):
                    self.get_cell(coord).set_player(' ')
                    self.get_cell(new_coord).set_player(num)
                    self.set_player_pos(num, new_coord)
                    return True
                else:
                    return False
            else:
                self.get_cell(coord).set_player(' ')
                self.get_cell(new_coord).set_player(num)
                self.set_player_pos(num, new_coord)
                return True
        else:
            return False
                                          
    def move_player(self, num, direction):
        """
            Déplace le joueur sur la glace
        """
        bPossible = True
        
        while bPossible:
            bPossible = self.move_player_one_time_by_direction(num, direction)
            
            if num == 0 :
                x, y = self.list_player[num]
                if self.get_cell((x,y)).is_final():
                    bPossible = False
            
    def move_player_at_coord(self, num, coord):
        """
            Utile pour le undo
        """
        x_act, y_act = self.list_player[num]
        x, y = coord
        
        self.set_player_pos(num, coord)
        
        self.get_cell((x_act,y_act)).set_player(' ')
        self.get_cell((x,y)).set_player(num)
            
    def is_over(self):
        """
            A-t-on gagné ?
        """
        x, y = self.list_player[0]
        
        return self.get_cell((x,y)).is_final()
    
    def __str__(self):
        """
            Affichage d'une grille
        """
        grid_to_str = '+'+'-+'*self.__width+'\n' # Ligne du haut
        
        w, h = self.get_grid_dimension()
        
        for y in range(h):
            
            grid_to_str += '|' # Nouvelle ligne
            
            for x in range(w):
                if (x,y) in self.list_player:
                    num = self.list_player.index((x,y))
                else:
                    num = ' '
                    
                if self.get_cell((x,y)).get_wall() & EAST_WALL:
                    grid_to_str += '{}|'.format(num)
                else:
                    grid_to_str += '{} '.format(num)
                    
            grid_to_str += '\n+'
            
            for x in range(w):
                bMod = False
                
                if self.get_cell((x,y)).get_wall() & SOUTH_WALL:
                    grid_to_str = grid_to_str[:-1] + '+-+'
                elif self.get_cell((x,y)).get_wall() & EAST_WALL:
                    grid_to_str += ' +'
                else:
                    if y < self.__height-1:
                        if self.get_cell((x,y+1)).get_wall() & EAST_WALL:
                            grid_to_str += ' +'
                            bMod = True
                    
                    if not bMod:
                        grid_to_str += '  '
            
            grid_to_str += '\n'
            
        return grid_to_str
    
    def from_file(filename):
        """
            Charger une grille depuis un fichier
        """
        
        dico_temp = {'N' : NORTH_WALL, 'E' : EAST_WALL,
                     'S' : SOUTH_WALL, 'W' : WEST_WALL}
        
        with open(filename) as f:
            # On retire les lignes avec des commentaires
            buff = [line[:-1] for line in f.readlines() if line[0] != '#']
            
            # Dimension de la grille
            dim = tuple([int(el) for el in buff[0].split(',')])
            buff = buff[1:] # On retire les dimensions
            grid = Grid(dim)
        
            # Case finale
            coord_final = tuple([int(el) for el in buff[0].split(',')])
            buff = buff[1:] # On retire les coordonnées de la case finale
            grid.set_final_case(coord_final)
            
            # Nombre de joueurs
            number_of_player = int(buff[0])
            buff = buff[1:] # On retire le nombre de joueur
            
            list_player = ()
            
            for _ in range(number_of_player):
                coord_player = tuple([int(el) for el in buff[0].split(',')])
                buff = buff[1:] # On retire les coordonnées de chaque joueur
                list_player = list_player + (coord_player,)
            
            # On ajoute les joueurs
            grid.set_config(list_player)
            
            # Reste les murs
            while buff != []:
                wall = buff[0].split(',')
                coord_wall = tuple([int(el) for el in wall[:2]])
                grid.set_case_wall(coord_wall, dico_temp[wall[2]])
                buff = buff[1:]
                    
            return grid