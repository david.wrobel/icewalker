from Grid import Grid
from Case import ALL_WALL, NORTH_WALL, EAST_WALL, SOUTH_WALL, WEST_WALL

class IceWalker():
    
    def __init__(self, grid):
        self.grid = grid
        self.played = [] # PILE pour la gestion des courps
        self.score = 0
        self.back = 0
    
    def undo(self):
        if self.played != []:            
            coup = self.played.pop()
            self.grid.move_player_at_coord(coup[0], coup[1])
            self.back += 1 
            
    def play(self, name = 'David'):
        self.name_player = name
        
        bContinue = True
        
        while bContinue and not(self.grid.is_over()):
            print(self.grid)
            
            if len(self.played) > 0:
                txt = input("{}, entrez votre mouvement 'num,direction', 'undo', ou 'q' (quit) : ".format(self.name_player))
            else:
                txt = input("{}, entrez votre mouvement 'num,direction' ou 'q' (quit) : ".format(self.name_player))
                
            if txt == 'q':
                bContinue = False
            elif txt == 'undo':
                self.undo()
            else:
                buff = txt.split(',')
                num = int(buff[0])
                direction = buff[1]
                
                # On sauvegarde les anciennes coordonnées du joueur
                self.played.append((num, self.grid.list_player[num]))
        
                self.grid.move_player(num, direction)
                self.score += 1
        
        if bContinue:
            print(self.grid)
            print('Bravoooooooooo !!!')
            print('Vous avez réussi en réalisant {} coups et {} retour(s) arrière.'.format(self.score, self.back))
    
    """
        Pour la résolution automatique
    """
    def get_possible_direction(self):
        """
            Retourne une liste du type
            [(0, 'N'), (0, 'E'), ..., (3, 'E')]
        """
        res = []
        wall = [NORTH_WALL, EAST_WALL, SOUTH_WALL, WEST_WALL]
        
        player = self.grid.get_config()
        num = 0
        
        for p in player:
            cell = self.grid.get_cell(p)
            free = ALL_WALL ^ cell.get_wall() # On récupère les possibilités de mouvement
            
            for m in wall:
                if free & m:
                    res.append((num, m))
                    
            num += 1
            
        return res
                               
    def play_auto(self):
        
        depart = self.grid.get_config()
                
        deja_vu = {depart : None}
        a_traiter = self.get_possible_direction()
        resolu = False

        while a_traiter != [] and not resolu:
            s = a_traiter[0]
            self.grid.move_player(s[0], s[1])
            
            if self.grid.is_over():
                resolu = True
            else:
                voisins = [self.grid.move_player(p[0], p[1]).get_config() for p in self.get_possible_direction()]
                for v in voisins:
                    if v not in deja_vu:
                        deja_vu[v] = s
                        a_traiter = a_traiter[1:]+v
                
        
        
