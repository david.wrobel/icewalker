"""
    Class Case():
    
    Autor : David Wrobel
    Date  : 07 Janvier 2020
    
    Implémentation d'une classe modélisant
    une case de jeu
"""

NONE_WALL  = 0b0000

NORTH_WALL = 0b0001
EAST_WALL  = 0b0010
SOUTH_WALL = 0b0100
WEST_WALL  = 0b1000

NW_CORNER  = NORTH_WALL | WEST_WALL
NE_CORNER  = NORTH_WALL | EAST_WALL
SW_CORNER  = SOUTH_WALL | WEST_WALL
SE_CORNER  = SOUTH_WALL | EAST_WALL

ALL_WALL   = NORTH_WALL | EAST_WALL | SOUTH_WALL | WEST_WALL

class Case():
    
    def __init__(self, wall = NONE_WALL, player = ' '):
        self.__wall = wall
        self.player = player
        self.final  = False
        
    def set_player(self, num):
        self.player = num
        
    def get_player(self):
        return self.player
    
    def set_final(self):
        self.final = True
        self.player = 'X'
        
    def is_free(self):
        return self.player == ' ' or self.player == 'X'
        
    def is_final(self):
        return self.final
    
    def set_wall(self, wall):
        self.__wall |= wall
        
    def get_wall(self):
        return self.__wall
        
        